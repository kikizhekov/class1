package com.example.class1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ContohController {

	@RequestMapping("/1aja") // untuk menamai url
	public String contoh() {
		return "/contoh"; // lokasi html file
	}
	
	@RequestMapping("/")
	public String utama() {
		return "/utama";
	}
	
}
