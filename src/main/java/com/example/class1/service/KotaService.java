package com.example.class1.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.class1.model.KotaModel;
import com.example.class1.repository.KotaRepository;

@Service
@Transactional
public class KotaService {

	@Autowired
	private KotaRepository kotaRepository;
	
	public void create(KotaModel kotaModel) {
		this.kotaRepository.save(kotaModel);
	}
	
	public Integer searchMaxId () {
		return this.kotaRepository.searchMaxId();
	}
	
	public List<KotaModel> read() {
		return this.kotaRepository.findAll();
	}
	
	public void delete(KotaModel kotamodel) {
		this.kotaRepository.deleteAll();
	}
	
	public KotaModel searchKodeKota(String kodeKota) {
		return this.kotaRepository.searchKodeKota(kodeKota);
	}
}
