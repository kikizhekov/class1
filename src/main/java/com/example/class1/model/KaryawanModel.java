package com.example.class1.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_KARYAWAN")
public class KaryawanModel {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) //untuk auto-generate(incremental angka)
	@Column(name = "ID_KRY")	
	private Integer idKaryawan;
	
	@Column (name = "KODE_KRY")
	private String kodeKaryawan;
	
	@Column (name = "NAMA_KRY")
	private String namaKaryawan;
	
	@Column(name = "TGL_LAHIR_KRY")
	private Date tglLahirKaryawan;
	
	@Column(name = "NAMA_FOTO")
	private String namaFoto;
	
	@Column (name = "TIPE_FOTO")
	private String tipeFoto;
	
	@Lob
	@Column (name = "FILE_FOTO")
	private byte[] fileFoto;
	
	@Column (name = "IS_DELETE", columnDefinition = "int default 0")
	private Integer isDelete;
	
	@Column(name = "ID_KOTA")
	private Integer idKota;
	
	@ManyToOne	
	@JoinColumn(name = "ID_KOTA", nullable=true, updatable=false, insertable=false)
	private KotaModel kotaModel;

	public Integer getIdKaryawan() {
		return idKaryawan;
	}

	public void setIdKaryawan(Integer idKaryawan) {
		this.idKaryawan = idKaryawan;
	}

	public String getKodeKaryawan() {
		return kodeKaryawan;
	}

	public void setKodeKaryawan(String kodeKaryawan) {
		this.kodeKaryawan = kodeKaryawan;
	}

	public String getNamaKaryawan() {
		return namaKaryawan;
	}

	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}

	public Date getTglLahirKaryawan() {
		return tglLahirKaryawan;
	}

	public void setTglLahirKaryawan(Date tglLahirKaryawan) {
		this.tglLahirKaryawan = tglLahirKaryawan;
	}
	
	public String getNamaFoto() {
		return namaFoto;
	}

	public void setNamaFoto(String namaFoto) {
		this.namaFoto = namaFoto;
	}

	public String getTipeFoto() {
		return tipeFoto;
	}

	public void setTipeFoto(String tipeFoto) {
		this.tipeFoto = tipeFoto;
	}

	public byte[] getFileFoto() {
		return fileFoto;
	}

	public void setFileFoto(byte[] fileFoto) {
		this.fileFoto = fileFoto;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getIdKota() {
		return idKota;
	}

	public void setIdKota(Integer idKota) {
		this.idKota = idKota;
	}

	public KotaModel getKotaModel() {
		return kotaModel;
	}

	public void setKotaModel(KotaModel kotaModel) {
		this.kotaModel = kotaModel;
	}		
}
