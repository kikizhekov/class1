package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "T_ANGGOTA")
public class AnggotaModel {

	@Id

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID_ANGGOTA" , nullable = false, length = 15 )
	private Integer idAnggota;
	
	@Column(name = "KD_ANGGOTA")
	private String kodeAnggota;
	
	@Column(name = "NM_ANGGOTA")
	private String namaAnggota;
	
	@Column(name = "USIA")
	private Integer usiaAnggota;
	
	@Column(name = "KD_FAKULTAS" , nullable = false, length = 10)
	private String kodeFakultas;
	
	@ManyToOne	
	@JoinColumn(name = "KD_FAKULTAS", nullable=true, updatable=false, insertable=false)
	private FakultasModel fakultasModel;
	
	public Integer getIdAnggota() {
		return idAnggota;
	}

	public void setIdAnggota(Integer idAnggota) {
		this.idAnggota = idAnggota;
	}

	public String getKodeAnggota() {
		return kodeAnggota;
	}

	public void setKodeAnggota(String kodeAnggota) {
		this.kodeAnggota = kodeAnggota;
	}

	public String getNamaAnggota() {
		return namaAnggota;
	}

	public void setNamaAnggota(String namaAnggota) {
		this.namaAnggota = namaAnggota;
	}

	public Integer getUsiaAnggota() {
		return usiaAnggota;
	}

	public void setUsiaAnggota(Integer usiaAnggota) {
		this.usiaAnggota = usiaAnggota;
	}

	public String getKodeFakultas() {
		return kodeFakultas;
	}

	public void setKodeFakultas(String kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}

	public FakultasModel getFakultasModel() {
		return fakultasModel;
	}

	public void setFakultasModel(FakultasModel fakultasModel) {
		this.fakultasModel = fakultasModel;
	}


}
