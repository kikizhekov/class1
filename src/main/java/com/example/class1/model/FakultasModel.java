package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_FAKULTAS")
public class FakultasModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "KD_FAKULTAS" , nullable = false, length = 10)
	private String kodeFakultas;
	
	@Column(name = "NM_FAKULTAS")
	private String namaFakultas;
	
	public String getKodeFakultas() {
		return kodeFakultas;
	}

	public void setKodeFakultas(String kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}

	public String getNamaFakultas() {
		return namaFakultas;
	}

	public void setNamaFakultas(String namaFakultas) {
		this.namaFakultas = namaFakultas;
	}
	
}
