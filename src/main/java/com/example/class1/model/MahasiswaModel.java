package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_JURUSAN")
public class MahasiswaModel {
	
	@Id
	@Column(name = "NM_JURUSAN" , nullable = true, length = 25)
	private String namaJurusan;
	
	@Column(name = "KD_FAKULTAS")
	private String kodeFakultas;
	
	@Column(name = "GRADE_FAKULTAS" , nullable = true, length = 2)
	private Integer gradeJurusan;
	
	@Column(name = "KD_JURUSAN" , nullable = false, length = 10)
	private String kodeJurusan;
	
	public String getKodeJurusan() {
		return kodeJurusan;
	}
	public void setKodeJurusan(String kodeJurusan) {
		this.kodeJurusan = kodeJurusan;
	}
	public String getNamaJurusan() {
		return namaJurusan;
	}
	public void setNamaJurusan(String namaJurusan) {
		this.namaJurusan = namaJurusan;
	}
	public String getKodeFakultas() {
		return kodeFakultas;
	}
	public void setKodeFakultas(String kodeFakultas) {
		this.kodeFakultas = kodeFakultas;
	}
	public Integer getGradeJurusan() {
		return gradeJurusan;
	}
	public void setGradeJurusan(Integer gradeJurusan) {
		this.gradeJurusan = gradeJurusan;
	}
	

}
